usage="\nRedbox Shortened URL Talend Job v1.2\n\nCreated and Maintained by Luigi Weiss\n\nFor assistance, please go to https://3cinteractive.atlassian.net/wiki/display/DT/Jobs%3A+Redbox+Shortened+URL+Talend+Job\n\n"

while getopts :h option
do
        case "${option}"
        in
                h) echo -e "$usage"
                                exit 0
                                ;;
        esac
done

cd `dirname $0`
 ROOT_PATH=`pwd`
 java -Xms256M -Xmx1024M -cp $ROOT_PATH/../lib/camel-core-2.10.4.jar:$ROOT_PATH/../lib/commons-lang-2.4.jar:$ROOT_PATH/../lib/commons-lang-2.6.jar:$ROOT_PATH/../lib/dom4j-1.6.1.jar:$ROOT_PATH/../lib/filecopy.jar:$ROOT_PATH/../lib/jakarta-oro-2.0.8.jar:$ROOT_PATH/../lib/json-path-0.8.1.jar:$ROOT_PATH/../lib/json-smart-1.1.1.jar:$ROOT_PATH/../lib/jxl.jar:$ROOT_PATH/../lib/mongo-java-driver-2.9.3.jar:$ROOT_PATH/../lib/mysql-connector-java-5.1.22-bin.jar:$ROOT_PATH/../lib/talend_file_enhanced_20070724.jar:$ROOT_PATH/../lib/talendcsv.jar:$ROOT_PATH:$ROOT_PATH/../lib/systemRoutines.jar::$ROOT_PATH/../lib/userRoutines.jar::.:$ROOT_PATH/url_short_1_3.jar: url_short.url_short_1_3.URL_Short --context=Stage "$@" 